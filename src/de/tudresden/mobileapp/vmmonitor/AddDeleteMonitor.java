package de.tudresden.mobileapp.vmmonitor;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddDeleteMonitor extends Activity {
	Activity context = this;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		final Activity context = this;
		SQLiteDatabase database;
		final EditText mName;
		final EditText mIP;

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_delete_monitor);
		mName = (EditText) findViewById(R.id.MonName);
		mIP = (EditText) findViewById(R.id.ipaddress);
		Button b = (Button) findViewById(R.id.AddMon);
		b.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Perform action on click
				String monName = mName.getText().toString();
				String monIP = mIP.getText().toString();
				// System.out.println("monaname==>" + monName);
				// System.out.println("monIP==>" + monIP);

				monitorDataDB db = new monitorDataDB(context);
				db.addMonitor(monName, monIP);
				// db.getNumMonitors();
				// db.createTable(monName);
				// db.getMonitors();
				// get json file from monitor
				// parse the jboss file and create a table and update the
				// contents
				// db.addProduct();
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_delete_monitor, menu);
		return true;
	}
}
