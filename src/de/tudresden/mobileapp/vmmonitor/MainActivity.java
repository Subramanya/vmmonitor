package de.tudresden.mobileapp.vmmonitor;

import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import de.tudresden.mobileapp.vmmonitor.businessobjects.Script;
import de.tudresden.mobileapp.vmmonitor.businessobjects.Server;
import de.tudresden.mobileapp.vmmonitor.configuration.Settings;

public class MainActivity extends Activity {
	ExpandableListView explvlist;
	ExpandableListView tmplvlist;
	WebView webview;
	Context context = this;
	int currentview = 0;

	TextView mDisplay;
	Button test1;
	GoogleCloudMessaging gcm;
	AtomicInteger msgId = new AtomicInteger();
	String regid;

	Bundle state;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		state = savedInstanceState;
		// System.out.println(savedInstanceState.toString());

		setContentView(R.layout.activity_main);

		monitorDataDB db = new monitorDataDB(context);
		// schedule();
		// db.dropMonTable();
		if (!db.checkTable("Monitors")) {
			alert();
		} else {
			int numVm = db.getNumMonitors();
			// int numVm = 0;
			View v = null;
			// createNotification(v);
			if (numVm > 0) {
				explvlist = (ExpandableListView) findViewById(R.id.ParentLevel);

				ExpandableListAdapter adt = new ParentLevel(numVm);
				explvlist.setAdapter(adt);
				// Testing GCM
				mDisplay = (TextView) findViewById(R.id.mDisplay);
			}
		}

		// Check device for Play Services APK. If check succeeds, proceed with
		// GCM registration.
		// Log.i(Settings.TAG, "Checking Play Services availability");
		// if (checkPlayServices()) {
		// gcm = GoogleCloudMessaging.getInstance(this);
		// regid = getRegistrationId(context);
		//
		// if (regid.isEmpty()) {
		// Log.i(Settings.TAG, "Registering google play service");
		// registerInBackground();
		// Log.i(Settings.TAG, "Registered google play service");
		// } else {
		// Log.i(Settings.TAG, "Already registerd google play service");
		// Log.i(Settings.TAG, "Register ID: " + regid);
		// }
		// } else {
		// Log.i(Settings.TAG, "No valid Google Play Services APK found.");
		// }
	}

	void alert() {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
		builder1.setMessage("Monitor does not exists,Please add monitor first");

		builder1.setCancelable(true);
		builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});

		AlertDialog alert11 = builder1.create();
		alert11.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, "Add Monitor");
		menu.add(0, 1, 0, "Sync Server");
		menu.add(0, 2, 0, "Register GCM");
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.i(Settings.TAG,
				"Calling meny item. Item selected ==> " + item.getItemId());

		switch (item.getItemId()) {
		case 0:
			monitorDataDB db1 = new monitorDataDB(context);
			System.out.println("item1 selected");

			db1.dropMonTable();
			// create monitor,vm,scripts,vmscript table
			db1.createTable("create table if not exists Monitors (MID integer PRIMARY KEY AUTOINCREMENT,MonName text not null,MIP integer,Location text not null )");
			Intent i = new Intent(getBaseContext(), AddDeleteMonitor.class);
			startActivity(i);
			return true;
		case 1:
			System.out.println("item2 selected");
			monitorDataDB db = new monitorDataDB(context);
			// drop old tables
			db.dropTables();

			List<Server> sl = null;
			db.createTables();

			// db.addElement();
			// db.getMonitors();

			ParseJSON pj = new ParseJSON();
			try {
				sl = pj.parseJSONData(new FetchServerData().getAllServers());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			db.addElement(sl);
			onCreate(state);
			// db.findVM("appserver0");
			// db.findVM("appserver1");
			return true;
		case 2:
			Log.i(Settings.TAG, "Item 3 selected");
			Intent intent = new Intent(getBaseContext(),
					RegistrationActivity.class);
			startActivity(intent);
			return true;
		default:
			return true;
		}

	}

	public class ParentLevel extends BaseExpandableListAdapter {
		int numMon;
		int j = 0;

		ParentLevel(int numM) {
			numMon = numM;

		}

		@Override
		public Object getChild(int arg0, int arg1) {
			return arg1;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		@Override
		public View getChildView(final int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
			CustExpListview SecondLevelexplv = new CustExpListview(
					MainActivity.this);
			// System.out.println("in secondlevel");
			SecondLevelexplv.setAdapter(new SecondLevelAdapter(
					groupPosition + 1, childPosition + 1));
			SecondLevelexplv.setGroupIndicator(null);
			SecondLevelexplv
					.setOnItemLongClickListener(new OnItemLongClickListener() {
						@Override
						public boolean onItemLongClick(AdapterView<?> parent,
								View view, int position, long id) {

//							ScheduledExecutorService scheduler = Executors
//									.newSingleThreadScheduledExecutor();
//
//							scheduler.scheduleAtFixedRate(new Runnable() {
//								public void run() {

									Intent i = new Intent(getBaseContext(),
											VMProperty.class);

									i.putExtra("keyName", String.valueOf(groupPosition+1));
									
									startActivity(i);
//									finish();
//								}
//							}, 0, 10, TimeUnit.SECONDS);
							return false;
						}

					});
			return SecondLevelexplv;

		}

		@Override
		public int getChildrenCount(int groupPosition) {
			monitorDataDB db = new monitorDataDB(context);
			return db.getNumSeversInMonitor(groupPosition + 1);

		}

		@Override
		public Object getGroup(int groupPosition) {
			return groupPosition;
		}

		@Override
		public int getGroupCount() {
			return numMon;
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			monitorDataDB db = new monitorDataDB(context);
			Server s = db.getMonitors(groupPosition + 1);

			TextView tv = new TextView(MainActivity.this);
			tv.setText(s.getName());
			tv.setTextColor(Color.WHITE);
			tv.setTextSize(20);

			tv.setBackgroundColor(Color.DKGRAY);

			tv.setPadding(10, 7, 7, 7);
			return tv;
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}
	}

	public class CustExpListview extends ExpandableListView {
		int intGroupPosition, intChildPosition, intGroupid;

		public CustExpListview(Context context) {
			super(context);
		}

		protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
			widthMeasureSpec = MeasureSpec.makeMeasureSpec(960,
					MeasureSpec.AT_MOST);
			heightMeasureSpec = MeasureSpec.makeMeasureSpec(600,
					MeasureSpec.AT_MOST);
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		}
	}

	public class SecondLevelAdapter extends BaseExpandableListAdapter {
		int MID;
		int VID;
		int numVms;
		Server server;
		List<Integer> sid;

		SecondLevelAdapter(int monId, int vmId) {
			MID = monId;
			VID = vmId;
			monitorDataDB db = new monitorDataDB(context);
			numVms = db.getNumSeversInMonitor(MID);
			sid = db.getScrIds(vmId);

		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			return childPosition;
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		@Override
		public View getChildView(final int groupPosition,
				final int childPosition, boolean isLastChild, View convertView,
				ViewGroup parent) {
			final Script scr;
			monitorDataDB db = new monitorDataDB(context);
			scr = db.getScriptInfo(VID, (Integer) sid.toArray()[childPosition]);

			TextView tv = new TextView(MainActivity.this);
			// System.out.println("File===>" + scr.getFile());
			tv.setText(scr.getName());

			tv.setTextColor(Color.BLACK);
			tv.setTextSize(20);
			tv.setPadding(10, 7, 7, 7);
			tv.setBackgroundColor(Color.LTGRAY);
			tv.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent i = new Intent(getBaseContext(), RunScript.class);
					String params = scr.getName() + "_" + VID + "_"
							+ scr.getId();
					i.putExtra("keyName", params);
					startActivity(i);
				}
			});
			// System.out.println("return");
			return tv;
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			monitorDataDB db = new monitorDataDB(context);
			return db.getNumScriptsInSever(VID);

		}

		@Override
		public Object getGroup(int groupPosition) {
			return groupPosition;
		}

		@Override
		public int getGroupCount() {
			return 1;
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			monitorDataDB db = new monitorDataDB(context);
			server = db.getVmInfo(VID);

			TextView tv = new TextView(MainActivity.this);
			tv.setText("Virtual Machine");
			tv.setTextColor(Color.BLACK);
			tv.setTextSize(20);
			tv.setPadding(10, 7, 7, 7);
			tv.setBackgroundColor(Color.GRAY);
			return tv;
		}

		@Override
		public boolean hasStableIds() {
			// TODO Auto-generated method stub
			return true;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			// TODO Auto-generated method stub
			return true;
		}
	}

	// Intent i = new Intent(this, ActivityTwo.class);
	// startActivity(intent);
	// //
	@Override
	public void onBackPressed() {

		if (currentview == 1) {
			currentview = 0;
			setContentView(R.layout.activity_main);

			// Change activity to previous view
		} else
			super.onBackPressed();
	}

	@Override
	protected void onResume() {
		super.onResume();
		// Check device for Play Services APK.
		// checkPlayServices();
	}

	/**
	 * Check the device to make sure it has the Google Play Services APK. If it
	 * doesn't, display a dialog that allows users to download the APK from the
	 * Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		System.out.println("Result Code: " + resultCode);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				Log.i(Settings.TAG,
						"This device Supported. And it is running :)");
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						Settings.PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i(Settings.TAG, "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}

	/**
	 * Gets the current registration ID for application on GCM service, if there
	 * is one.
	 * <p>
	 * If result is empty, the app needs to register.
	 * 
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	private String getRegistrationId(Context context) {
		final SharedPreferences prefs = getGcmPreferences(context);
		String registrationId = prefs.getString(Settings.PROPERTY_REG_ID, "");
		if (registrationId.isEmpty()) {
			Log.i(Settings.TAG, "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(Settings.PROPERTY_APP_VERSION,
				Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i(Settings.TAG, "App version changed.");
			return "";
		}
		return registrationId;
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private SharedPreferences getGcmPreferences(Context context) {
		// This sample app persists the registration ID in shared preferences,
		// but
		// how you store the regID in your app is up to you.
		return getSharedPreferences(MainActivity.class.getSimpleName(),
				Context.MODE_PRIVATE);
	}

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and the app versionCode in the application's
	 * shared preferences.
	 */
	private void registerInBackground() {
		Log.i(Settings.TAG, "Registring cellphone");
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(context);
					}
					regid = gcm.register(Settings.GCM_SENDER);
					Log.i(Settings.TAG,
							"Cellphone registered with cellphone id: " + regid);
					msg = "Device registered, registration ID=" + regid;
					System.out.println("MSG:" + msg);
					sendRegistrationIdToBackend();
					storeRegistrationId(context, regid);
				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
				}
				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
			}
		}.execute(null, null, null);
	}

	private void unregisterInBackground() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(context);
					}
					gcm.unregister();
					System.out.println("MSG: Device unregistered!!!" + msg);
					regid = null;
					storeRegistrationId(context, regid);
				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
				}
				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
			}
		}.execute(null, null, null);
	}

	/**
	 * Stores the registration ID and the app versionCode in the application's
	 * {@code SharedPreferences}.
	 * 
	 * @param context
	 *            application's context.
	 * @param regId
	 *            registration ID
	 */
	private void storeRegistrationId(Context context, String regId) {
		final SharedPreferences prefs = getGcmPreferences(context);
		int appVersion = getAppVersion(context);
		Log.i(Settings.TAG, "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(Settings.PROPERTY_REG_ID, regId);
		editor.putInt(Settings.PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}

	/**
	 * Sends the registration ID to your server over HTTP, so it can use
	 * GCM/HTTP or CCS to send messages to your app. Not needed for this demo
	 * since the device sends upstream messages to a server that echoes back the
	 * message using the 'from' address in the message.
	 */
	private void sendRegistrationIdToBackend() {
		Log.i(Settings.TAG, "Registering device");
		String deviceID = Secure.getString(context.getContentResolver(),
				Secure.ANDROID_ID);
		Device.register(deviceID, regid);
		Log.i(Settings.TAG, "Device registered");
	}

}
