package de.tudresden.mobileapp.vmmonitor.businessobjects;

import java.util.List;

public class Server {
	
	private Long id;
	private String ip;
	private String location;
	private String name;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	private List<Script> scripts;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public List<Script> getScripts() {
		return scripts;
	}
	public void setScripts(List<Script> scripts) {
		this.scripts = scripts;
	}

}
