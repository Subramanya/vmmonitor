package de.tudresden.mobileapp.vmmonitor;

import java.util.ArrayList;
import java.util.List;

import de.tudresden.mobileapp.vmmonitor.businessobjects.Script;
import de.tudresden.mobileapp.vmmonitor.businessobjects.Server;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class monitorDataDB extends SQLiteOpenHelper {

	public static final String tableName = "Monitor";
	public static final String vmName = "vmName";
	public static final String vmID = "VID";
	public static final String vmIP = "VIP";
	public static final String scriptName = "ScName";
	public static final String scriptID = "SID";

	private static final String DATABASE_NAME = "vmmon.db";
	private static final int DATABASE_VERSION = 1;

	// Database creation sql statement

	public monitorDataDB(Context context) {

		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public void createTables() {

		// create monitor,vm,scripts,vmscript table
		createTable("create table if not exists Monitors ( MonName text not null ,MID integer ,MIP integer,Location text not null )");
		createTable("create table if not exists Vms (MID integer,vmname text not null ,VID integer ,VIP integer,Location text not null )");
		createTable("create table if not exists Scripts   (SID integer, scriptname text not null,file text not null )");
		createTable("create table if not exists VmScripts ( VID integer ,SID integer  )");

	}

	public void createResTableStoreData(int id, String data) {
		createTable("create table if not exists scrresult ( SID integer,Result text not null )");
		ContentValues values = new ContentValues();
		values.put("SID", id);
		SQLiteDatabase db = this.getWritableDatabase();
		values.put("Result", data);

		db.insert("scrresult", null, values);
		db.close();

	}

	public String getScriptDataAndDelete(int id) {

		String query = "Select * from scrresult where SID=" + id;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor c = db.rawQuery(query, null);
		c.moveToFirst();

		String result = c.getString(1);
		db.execSQL("DROP TABLE IF EXISTS scrresult");
		db.close();
		return result;
	}

	public void createTable(String query) {
		SQLiteDatabase db = this.getWritableDatabase();

		// final String DATABASE_CREATE = "create table " + tableName + "("
		// + vmName + " text not null , " + vmID + " integer , " + vmIP
		// + " text not null , " + scriptName + " text not null ,"
		// + scriptID + " integer " + ");";
		db.execSQL(query);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		// final String DATABASE_CREATE = "create table " + tableName
		// + "(" + vmName + " text not null , " + vmID + " integer , " + vmIP
		// + " text not null , " + scriptName + " text not null ," + scriptID
		// + " integer " + ");";
		// database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(monitorDataDB.class.getName(), "Upgrading database from version "
				+ oldVersion + " to " + newVersion
				+ ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + tableName);
		onCreate(db);
	}

	public void addMonitor(String MonName, String MonIp) {
		SQLiteDatabase db = this.getWritableDatabase();
		// db.execSQL("DROP TABLE IF EXISTS Monitors");
		ContentValues values = new ContentValues();
		values.put("MonName", MonName);
		// values.put("MID", Mid);
		values.put("MIP", MonIp);
		values.put("Location", "Dresden");
		db.insert("Monitors", null, values);

	}

	public void addElement(List<Server> sl) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();

		for (int i = 0; i < sl.size(); i++) {
			Server s = sl.get(i);
			values = new ContentValues();
			values.put("MID", 1);
			values.put("vmname", "appserver" + i);
			values.put("VID", s.getId());
			values.put("VIP", s.getIp());
			values.put("location", s.getLocation());

			db.insert("Vms", null, values);
			List<Script> scl = s.getScripts();
			for (int j = 0; j < scl.size(); j++) {
				values = new ContentValues();
				Script sc = scl.get(j);
				values.put("scriptName", sc.getName());
				values.put("SID", sc.getId());
				values.put("file", sc.getFile());
				System.out.println(sc.getName() + sc.getId() + sc.getFile());
				db.insert("Scripts", null, values);

				values = new ContentValues();
				values.put("VID", s.getId());
				values.put("SID", sc.getId());
				db.insert("VmScripts", null, values);
			}

		}

		db.close();
	}

	public int getNumSeversInMonitor(int monId) {
		String query = "Select * from Vms where MID=" + monId;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor c = db.rawQuery(query, null);
		c.getCount();
		// System.out.println("num of vms====>" + );
		db.close();
		return c.getCount();
	}

	public int getNumScriptsInSever(int serId) {
		String query = "Select * from VmScripts  where VID=" + serId;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor c = db.rawQuery(query, null);
		// System.out.println("num of scripts====>" + c.getCount());
		int count = c.getCount();
		db.close();
		return count;
	}

	public Server getVmInfo(int serId) {
		String query = "Select * from Vms  where VID=" + serId;
		// System.out.println("MONID===>" + serId);
		Server s = new Server();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor c = db.rawQuery(query, null);
		if (c.moveToFirst()) {
			while (!c.isAfterLast()) {
				s.setName(c.getString(1));
				s.setId(c.getLong(2));
				s.setIp(c.getString(1));
				//
				// System.out.println("MONID===>" + c.getLong(2));
				// System.out.println("MONIP===>" + c.getString(1));
				c.moveToNext();
			}

		}
		return s;
	}

	public Script getScriptInfo(int vID, int sid) {

		// String query = "SELECT * FROM Scripts where Scripts as s;
		String query = "Select * from Scripts s,vmScripts v where (s.SID = v.SID  and  ( v.VID ="
				+ vID + " and v.SID=" + sid + ") )";
		// System.out.println("MONID===> " + sid + " " + vID);
		Script s = new Script();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor c = db.rawQuery(query, null);
		if (c.moveToFirst()) {
			while (!c.isAfterLast()) {
				s.setName(c.getString(1));
				s.setId(c.getLong(0));
				s.setFile(c.getString(2));
				c.moveToNext();
			}

		}
		return s;
	}

	public Cursor getElement(String query) {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor c = db.rawQuery(query, null);
		return c;
	}

	public List<Integer> getScrIds(int VID) {
		SQLiteDatabase db = this.getWritableDatabase();
		List<Integer> sid = new ArrayList<Integer>();
		String query = "Select SID FROM  VmScripts where VID=" + VID;
		Cursor cursor;
		cursor = db.rawQuery(query, null);
		if (cursor.moveToFirst()) {

			while (cursor.isAfterLast() == false) {
				sid.add(cursor.getInt(0));
				// System.out.println("VMscID===>" + cursor.getLong(0));
				cursor.moveToNext();
			}
		}
		return sid;
	}

	public void deleteElement(String query) {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor c = db.rawQuery(query, null);

	}

	public Server getMonitors(int mID) {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM Monitors where MID=" + mID, null);
		Server ser = new Server();
		if (c.moveToFirst()) {
			while (!c.isAfterLast()) {
				c.getString(0);
				ser.setName(c.getString(1));
				ser.setIp(c.getString(2));
				// System.out.println("MONNAME===>" + );
				// System.out.println("MONID===>" + c.getString(1));
				// System.out.println("MONIP===>" + c.getString(2));
				c.moveToNext();
			}
		}
		return ser;

	}

	public void deleteMonitor(String tName) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DROP TABLE IF EXISTS" + tName);
		db.close();
	}

	public void dropTables() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DROP TABLE IF EXISTS Scripts");
		//
		// db.execSQL("DROP TABLE IF EXISTS Monitor");
		db.execSQL("DROP TABLE IF EXISTS VmScripts");
		db.execSQL("DROP TABLE IF EXISTS Vms");

		db.close();
	}

	public void dropMonTable() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DROP TABLE IF EXISTS Monitors");
	}

	public void findVM(String productname) {

		String query = "Select * FROM  Vms";
		SQLiteDatabase db = this.getWritableDatabase();

		Cursor cursor = db.rawQuery(query, null);
		// System.out.println("rows===>" + cursor.getCount());
		if (cursor.moveToFirst()) {

			while (cursor.isAfterLast() == false) {

				System.out.println("vmName===>" + cursor.getString(1));
				System.out.println("vmID===>" + cursor.getString(2));
				System.out.println("vmIP===>" + cursor.getString(3));
				System.out.println("sName===>" + cursor.getString(4));
				cursor.moveToNext();
			}
		}

		query = "Select * FROM  Scripts";
		cursor = db.rawQuery(query, null);

		if (cursor.moveToFirst()) {

			while (cursor.isAfterLast() == false) {

				System.out.println("SCName===>" + cursor.getString(1));
				System.out.println("SCID===>" + cursor.getString(0));
				System.out.println("SCF===>" + cursor.getString(2));
				cursor.moveToNext();
			}
		}

		query = "Select * FROM  VmScripts";
		cursor = db.rawQuery(query, null);
		if (cursor.moveToFirst()) {

			while (cursor.isAfterLast() == false) {

				System.out.println("VMscID===>" + cursor.getLong(0));
				System.out.println("SCscID===>" + cursor.getInt(1));
				cursor.moveToNext();
			}
		}

		db.close();
		cursor.close();
		// return product;
	}

	public int getNumMonitors() {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM Monitors", null);
		// System.out.println("num mon===>" + c.getCount());
		return c.getCount();

	}

	public boolean checkTable(String tabName) {
		SQLiteDatabase db = this.getWritableDatabase();
		try {
			// db.rawQuery(
			// "SELECT name FROM sqlite_master where type='table' and name="
			// + tabName, null);
			db.rawQuery("SELECT * FROM  " + tabName, null);
			return true;
		} catch (SQLException e) {
			return false;

		}

	}

}